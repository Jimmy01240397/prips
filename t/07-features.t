#!/bin/sh
#
# Copyright (c) 2021  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

if [ -f 'tap-functions.sh' ]; then
	. tap-functions.sh
elif [ -f 't/tap-functions.sh' ]; then
	. t/tap-functions.sh
else
	echo 'Bail out! Could not find tap-functions.sh'
	exit 99
fi

[ -z "$PRIPS" ] && PRIPS='./prips'

if ! feature-check feature-check feature-check 2>/dev/null; then
	skip_all_ 'The feature-check tool does not seem to be present'
fi

if [ "${PRIPS#*[$IFS]}" != "$PRIPS" ]; then
	skip_all_ 'PRIPS does not specify a single program path or name'
fi

plan_ 5

v=`feature-check -v $PRIPS prips`
res="$?"
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ -n "$v" ]; then ok_; else not_ok_ "empty 'prips' feature version"; fi
prips_version="$v"

v=`feature-check $PRIPS 'prips >= 1.2.0'`
res="$?"
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi

if feature-check -l $PRIPS | grep -Eqe '^prips-impl-'; then ok_; else not_ok_ 'no prips-impl-* features'; fi
if feature-check -l $PRIPS | awk -v expected="$prips_version" '/^prips-impl-/ { if ($2 != expected) { exit(1); } }'; then ok_; else not_ok_ 'the prips-impl-* features must have the same version as prips'; fi
