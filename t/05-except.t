#!/bin/sh
#
# Copyright (c) 2018, 2021  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

if [ -f 'tap-functions.sh' ]; then
	. tap-functions.sh
elif [ -f 't/tap-functions.sh' ]; then
	. t/tap-functions.sh
else
	echo 'Bail out! Could not find tap-functions.sh'
	exit 99
fi

[ -z "$PRIPS" ] && PRIPS='./prips'

if ! $PRIPS --features | grep -Fqe ' exclude='; then
	skip_all_ 'This prips implementation does not support exclusion'
fi
if ! $PRIPS --features | grep -Fqe ' exclude=1.'; then
	bailout_ 'Unsupported version of the "exclude" feature'
fi

plan_ 16

v=`$PRIPS -d33 1.2.3.24/29`
res="$?"
exp='1.2.3.24!1.2.3.25!1.2.3.26!1.2.3.27!1.2.3.28!1.2.3.29!1.2.3.30!1.2.3.31!'
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ "$v" = "$exp" ]; then ok_; else not_ok_ "expected $exp got $v"; fi

v=`$PRIPS -d33 -e ...28 1.2.3.24/29`
res="$?"
exp='1.2.3.24!1.2.3.25!1.2.3.26!1.2.3.27!1.2.3.29!1.2.3.30!1.2.3.31!'
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ "$v" = "$exp" ]; then ok_; else not_ok_ "expected $exp got $v"; fi

v=`$PRIPS -d33 -e ...28 -i 2 1.2.3.24/29`
res="$?"
exp='1.2.3.24!1.2.3.26!1.2.3.30!'
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ "$v" = "$exp" ]; then ok_; else not_ok_ "expected $exp got $v"; fi

v=`$PRIPS -d33 -e ...28 -i 4 1.2.3.24/29`
res="$?"
exp='1.2.3.24!'
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ "$v" = "$exp" ]; then ok_; else not_ok_ "expected $exp got $v"; fi

v=`$PRIPS -d33 -i 256 1.2.0.0/22`
res="$?"
exp='1.2.0.0!1.2.1.0!1.2.2.0!1.2.3.0!'
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ "$v" = "$exp" ]; then ok_; else not_ok_ "expected $exp got $v"; fi

v=`$PRIPS -d33 -i 256 -e ..1 1.2.0.0/22`
res="$?"
exp='1.2.0.0!1.2.2.0!1.2.3.0!'
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ "$v" = "$exp" ]; then ok_; else not_ok_ "expected $exp got $v"; fi

v=`$PRIPS -i 256 -e .0.. 1.0.0.0/8 | grep -Ece '\.[1-9][0-9]*$'`
res="$?"
exp='0'
if [ "$res" != 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ "$v" = "$exp" ]; then ok_; else not_ok_ "expected $exp got $v"; fi

v=`$PRIPS -d33 -i32 -e ...32 -e ...96 -e ..1. 4.0.0.0 4.0.2.193`
res="$?"
exp='4.0.0.0!4.0.0.32!4.0.0.64!4.0.0.96!4.0.0.128!4.0.0.160!4.0.0.192!4.0.0.224!4.0.2.0!4.0.2.32!4.0.2.64!4.0.2.96!4.0.2.128!4.0.2.160!4.0.2.192!'
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ "$v" = "$exp" ]; then ok_; else not_ok_ "expected $exp got $v"; fi
