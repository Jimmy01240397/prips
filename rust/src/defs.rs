//! Common definitions for the IP range output tool.
/*
 * Copyright (c) 2021  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::str::FromStr;

/// An inclusive IPv4 address range.
#[derive(Debug)]
pub struct AddrRange {
    /// The first address in the range.
    pub start: u32,
    /// The last address in the range.
    pub end: u32,
}

/// The address formatting mode.
#[derive(Debug)]
pub enum AddrFormat {
    /// Dotted-quad.
    Dot,
    /// A single decimal number.
    Dec,
    /// A single hexadecimal number.
    Hex,
}

quick_error! {
    /// An error that occurred during parsing the format mode.
    #[derive(Debug)]
    pub enum FormatError {
        /// An unknown formatting mode.
        Unknown {
            display("Unrecognized formatting mode")
        }
    }
}

impl FromStr for AddrFormat {
    type Err = FormatError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "dot" => Ok(Self::Dot),
            "dec" => Ok(Self::Dec),
            "hex" => Ok(Self::Hex),
            _ => Err(Self::Err::Unknown),
        }
    }
}

/// The program operation mode: list addresses or do something else.
#[derive(Debug)]
pub enum Mode {
    /// Display usage or version information, already done.
    Done,
    /// List the addresses in the specified range.
    List,
    /// Represent a range in CIDR form.
    Cidrize,
}

/// Runtime configuration for the IP range output tool.
#[derive(Debug)]
pub struct Config {
    /// The delimiter between two successive IP addresses.
    pub delim: char,
    /// The address formatting mode.
    pub format: AddrFormat,
    /// The program operation mode.
    pub mode: Mode,
    /// The address range to process.
    pub range: AddrRange,
    /// The loop step.
    pub step: usize,
}
