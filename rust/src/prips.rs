//! Output a range of IP addresses.
/*
 * Copyright (c) 2021  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use crate::defs;

fn format_dot(addr: u32, delim: char) {
    print!(
        "{}.{}.{}.{}{}",
        addr >> 24,
        (addr >> 16) & 255,
        (addr >> 8) & 255,
        addr & 255,
        delim,
    )
}

fn format_dec(addr: u32, delim: char) {
    print!("{}{}", addr, delim);
}

fn format_hex(addr: u32, delim: char) {
    print!("{:x}{}", addr, delim);
}

/// Output a range of IP addresses.
pub fn output_range(cfg: defs::Config) {
    let formatter = match cfg.format {
        defs::AddrFormat::Dot => format_dot,
        defs::AddrFormat::Dec => format_dec,
        defs::AddrFormat::Hex => format_hex,
    };
    for addr in (cfg.range.start..=cfg.range.end).step_by(cfg.step) {
        formatter(addr, cfg.delim);
    }
}

/// Convert a range into CIDR form.
pub fn cidrize(cfg: defs::Config) {
    let diff = cfg.range.start ^ cfg.range.end;
    let iter = (1u32..=32).scan(diff, |state, x| match *state {
        0 => None,
        value => {
            *state = value >> 1;
            Some(x)
        }
    });
    let (base, prefixlen) = match iter.last() {
        None => (cfg.range.start, 32),
        Some(32) => (0, 0),
        Some(offset) => ((cfg.range.start >> offset) << offset, 32 - offset),
    };
    format_dot(base, '/');
    println!("{}", prefixlen);
}
